package com.uaint.field;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.uaint.field.users.CurrentUser;
import com.uaint.field.users.FirebaseUserDriver;

import java.util.ArrayList;

public class TeamSelectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_select);
        // Modyfikowanie paska narzędzi - wywoływanie metody na centrowanie, ustawianie tytułu
        centerTitle();
        getSupportActionBar().setTitle(getString(R.string.game_code) + ": " + CurrentUser.getInstance().getJoinCode());
    }
    // Obsługa kliknięcia przycisku wybrania obserwatora
    public void SpectatorClick (View v){
        CurrentUser.getInstance().setTeamID(0); // Tryb obserwatora
        setTeamPath();
        startActivity(new Intent(this, MapsActivity.class));
    }
    // Obsługa kliknięcia przycisku wybrania czeronej drużyny
    public void RedClick(View view) {
        CurrentUser.getInstance().setTeamID(1); // Czerwona drużyna
        setTeamPath();
        startActivity(new Intent(this, MapsActivity.class));
    }
    // Obsługa kliknięcia przycisku wybrania niebieskiej drużyny
    public void BlueClick(View view) {
        CurrentUser.getInstance().setTeamID(2); // Niebieska drużyna
        setTeamPath();
        startActivity(new Intent(this, MapsActivity.class));
    }
    // Innowacyjna metoda do centrowania tekstu na pasku narzędzi bez potrzeby chowania systemowego i dodawania niestandardowego
    private void centerTitle() {
        ArrayList<View> textViews = new ArrayList<>();
        getWindow().getDecorView().findViewsWithText(textViews, getTitle(), View.FIND_VIEWS_WITH_TEXT);
        if(textViews.size() > 0) {
            AppCompatTextView appCompatTextView = null;
            if(textViews.size() == 1) {
                appCompatTextView = (AppCompatTextView) textViews.get(0);
            } else {
                for(View v : textViews) {
                    if(v.getParent() instanceof Toolbar) {
                        appCompatTextView = (AppCompatTextView) v;
                        break;
                    }
                }
            }
            if(appCompatTextView != null) {
                ViewGroup.LayoutParams params = appCompatTextView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                appCompatTextView.setLayoutParams(params);
                appCompatTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                appCompatTextView.setPadding(0,0,100,0);
            }
        }
    }

    private void setTeamPath() {
        FirebaseUserDriver.setTeamPath(CurrentUser.getInstance().getTeamID_S(),false);
        try {
            Log.i("TeamSelectActivity, FirebaseUserDriver for team select path: ", FirebaseUserDriver.getTeamPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}