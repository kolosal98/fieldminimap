package com.uaint.field;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class SmacznySnackbar {
    private Context context;
    private View view;
    public int dlugosc = BaseTransientBottomBar.LENGTH_SHORT;
    private Snackbar snackbar_smaczny;

    public SmacznySnackbar(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    public void setDlugosc(int dlugosc) {
        this.dlugosc = dlugosc;
    }

    public void consume() {
        this.snackbar_smaczny.dismiss();
    }

    public void MakeSmaczny (int kolorek, int ikonka, int tekscior) {
        this.snackbar_smaczny = Snackbar.make(view, tekscior, dlugosc);   // Wołanie Snackbara z tekstem (tekscior)
        View view_smaczny = snackbar_smaczny.getView(); // Przypisanie Snackbara
        TextView textview_smaczny = view_smaczny.findViewById(com.google.android.material.R.id.snackbar_text);   // Przypisywanie tekstu w Snackbarze
        FrameLayout.LayoutParams params_smaczny = (FrameLayout.LayoutParams)view_smaczny.getLayoutParams();    // Interpretacja przypisania jako oddzielny FrameLayout
        params_smaczny.gravity = Gravity.TOP;  // Ustawienie Snackbar na górze wyświetlacza
        view_smaczny.setLayoutParams(params_smaczny); // Przekazanie powyższego ustawienia do Snackbara
        view_smaczny.setBackgroundColor(ContextCompat.getColor(context, kolorek));   // Ustawienie koloru tła Snackbarze (kolorek)
        textview_smaczny.setCompoundDrawablesWithIntrinsicBounds(ikonka, 0, 0, 0);  // Ikona obok tekstu (ikonka)
        textview_smaczny.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);   // Centrowanie tekstu w Snackbarze
        textview_smaczny.setTypeface(null, Typeface.BOLD);   // Pogrubienie tekstu w Snackbarze
        textview_smaczny.setTextColor(ContextCompat.getColor(context, R.color.white)); // Ustawienie białego koloru tekstu w Snackbarze
        snackbar_smaczny.show();  // Wyświetlenie Snackbara
        Vibrator vibrator_smaczny = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);  // Przypisanie wibracji
        vibrator_smaczny.vibrate(100);  // Określenie długości wibracji
    }
}