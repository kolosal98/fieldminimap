package com.uaint.field;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uaint.field.databinding.ActivityMapsBinding;
import com.uaint.field.users.CurrentUser;
import com.uaint.field.users.FirebaseUserDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;
    private LocationManager locationManager;
    private double currentLatitude, currentLongitude;
    private static boolean wereActive = false;
    public static boolean stopGPS = false;
    private boolean follow = true;

    private static final int PERMISSION_REQUEST_CODE = 200;

    // Markery
    private Marker marker1;
    private Marker marker2;
    private Marker marker3;
    private Marker marker4;
    private Marker marker5;

    // Firebase
    @Deprecated
    public final FirebaseDatabase database = FirebaseDatabase.getInstance("https://field-c9fb0-default-rtdb.europe-west1.firebasedatabase.app/");

    // Debug - wyświetlanie
    public String[] usr0parts = new String[]{"0", "0"};
    public String[] usr1parts = new String[]{"0", "0"};
    public String[] usr2parts = new String[]{"0", "0"};
    public String[] usr3parts = new String[]{"0", "0"};

    // TODO Piersze odpalenie aplikacji zwraca błąd: Unable to start activity ComponentInfo{com.uaint.field/com.uaint.field.MapsActivity}: java.lang.SecurityException: uid 10433 does not have android.permission.ACCESS_COARSE_LOCATION or android.permission.ACCESS_FINE_LOCATION.

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Zatrzymuje GPS na wyjściu
        stopGPS = false;

        // Ładowanie mapy wygenerowane przez szablon
        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Definiowanie menedżera lokalizacji
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // Zapytanie o pozwolenie na używanie GPS
//        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},1);
//        }
        // Przygotowanie Firebase
        if (!wereActive) {
            findSetReserveFirstEmptyUserId();
            wereActive = true;
        }
        // Obsługa tworzenia Snackbaru oczekiwania na lokalizację
        SmacznySnackbar smacznySnackbar = new SmacznySnackbar(this, findViewById(android.R.id.content));
        smacznySnackbar.setDlugosc(BaseTransientBottomBar.LENGTH_INDEFINITE);
        switch (CurrentUser.getInstance().getTeamID_S()) {
            case "Spectator":
                smacznySnackbar.MakeSmaczny(R.color.turquoise, R.drawable.ic_baseline_my_location_24, R.string.waiting_for_location);
                break;
            case "TeamA":
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_my_location_24, R.string.waiting_for_location);
                break;
            case "TeamB":
                smacznySnackbar.MakeSmaczny(R.color.blue, R.drawable.ic_baseline_my_location_24, R.string.waiting_for_location);
                break;
            default:
                break;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, location -> {
            // TODO zwiększenie precyzji lokalizacji, wymuszenie/weryfikacja czy lokalizacja jest włączona.

            // Upewnia się, aby dane nie zostały zapisane po wyjściu do wyboru drużyn
            if (stopGPS) {
                return;
            }
            // Podpięcie Firebase i aktualizacja GPS w bd
            DatabaseReference firebaseReference = database.getReference("Group");
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("/" + CurrentUser.getInstance().getTeamID_S() + "/" + CurrentUser.getInstance().getUserID() + "/GPS", currentLatitude + "|" + currentLongitude);
            hashMap.put("/Spectator/" + CurrentUser.getInstance().getUserID() + "/GPS", currentLatitude + "|" + currentLongitude);
            firebaseReference.child(CurrentUser.getInstance().getJoinCode()).updateChildren(hashMap);
            // Parametry lokalizatora
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            LatLng current = new LatLng(currentLatitude, currentLongitude);
            marker1.setPosition(current);
            marker1.setVisible(true);
            smacznySnackbar.consume();
            // Dopasowanie ikon graczy do wybranej drużyny
            switch (CurrentUser.getInstance().getTeamID_S()) {
                case "Spectator":
                    marker1.setVisible(false);
                    break;
                case "TeamA":
                    marker1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.player_red_32));
                    break;
                case "TeamB":
                    marker1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.player_blue_32));
                    break;
                default:
                    break;
            }
            // Jeżeli funkcja śledzenia jest włączona, przenieś kamerę przy zmianie lokalizacji
            if (follow) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, 18));
            }
            // CURSED Metoda na wyświetlanie
            if (!(usr0parts[0] == null || usr0parts[1] == null)) {
                LatLng usr0 = new LatLng(Double.parseDouble(usr0parts[0]), (Double.parseDouble(usr0parts[1])));
                marker2.setPosition(usr0);
            }
            if (!(usr1parts[0] == null || usr1parts[1] == null)) {
                LatLng usr1 = new LatLng(Double.parseDouble(usr1parts[0]), (Double.parseDouble(usr1parts[1])));
                marker3.setPosition(usr1);
            }
            if (!(usr2parts[0] == null || usr2parts[1] == null)) {
                LatLng usr2 = new LatLng(Double.parseDouble(usr2parts[0]), (Double.parseDouble(usr2parts[1])));
                marker4.setPosition(usr2);
            }
            if (!(usr2parts[0] == null || usr2parts[1] == null)) {
                LatLng usr3 = new LatLng(Double.parseDouble(usr3parts[0]), (Double.parseDouble(usr3parts[1])));
                marker5.setPosition(usr3);
            }
        });
        // CURSED reference dla wyświetlania
        DatabaseReference firebaseReferenceDeep = database.getReference("Group/"+ CurrentUser.getInstance().getJoinCode()+"/Spectator/");
        // Cursed wczytywanie innych osób
        firebaseReferenceDeep.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!CurrentUser.getInstance().getUserID().equals("User0")) {
                    String user0 = (String) snapshot.child("User0/GPS").getValue();
                    if (user0 != null) {
                        usr0parts = user0.split("\\|");
                    }
                } else marker2.setVisible(false);
                if (!CurrentUser.getInstance().getUserID().equals("User1")) {
                    String user1 = (String) snapshot.child("User1/GPS").getValue();
                    if (user1 != null) {
                        usr1parts = user1.split("\\|");
                    }
                } else marker3.setVisible(false);
                if (!CurrentUser.getInstance().getUserID().equals("User2")) {
                    String user2 = (String) snapshot.child("User2/GPS").getValue();
                    if (user2 != null) {
                        usr2parts = user2.split("\\|");
                    }
                } else marker4.setVisible(false);
                if (!CurrentUser.getInstance().getUserID().equals("User3")) {
                    String user3 = (String) snapshot.child("User3/GPS").getValue();
                    if (user3 != null) {
                        usr3parts = user3.split("\\|");
                    }
                } else marker5.setVisible(false);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
        // Kolorowanie interfejsu i uzupełnianie jego wartości
        Button sTeam = findViewById(R.id.selected_team);
        switch(CurrentUser.getInstance().getTeamID_S()) {
            case "Spectator":
                paintColor(R.color.turquoise);
                sTeam.setText(getString(R.string.join_spectator_short));
                break;
            case "TeamA":
                paintColor(R.color.red);
                sTeam.setText(getString(R.string.join_red_short));
                break;
            case "TeamB":
                paintColor(R.color.blue);
                sTeam.setText(getString(R.string.join_blue_short));
                break;
            default:
                break;
        }
    }

    // Obsługa zdarzeń podczas odrzucenia permisji
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (grantResults.length > 0) {
                boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (!locationAccepted) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        // TODO Zangloszczyć
                        showMessageOKCancel("Aby aplikacja działała poprawnie należy zezwolić na korzystanie z dokładnej lokalizacji.", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
                            }
                        }, (dialogInterface, i) -> finish());

                        // TODO W else zrobić snackbar z hiperlinkiem do ustawień gdzie można zmienić permisje
                    } else {
                        if (getApplicationContext() == null) {
                            return;
                        }
                        // Otwórz ustawienia, aby zachęcić użytkownika do włączenia permisjii // TODO fajnie by było pokazać gdzie klikać, ale nie znalazłem rozwiązania na necie - nie wiem jak szukać.
                        final Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        getApplicationContext().startActivity(i);
                    }
                }
            }
    }



    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener noListener) {
        new AlertDialog.Builder(MapsActivity.this)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okListener)
                .setNegativeButton(android.R.string.cancel, noListener)
                .create()
                .show();
    }


    // Przełączanie funkcji śledzenia
    public void FollowMethod(View view) {
        FloatingActionButton guiFAB = findViewById(R.id.floating_follow);
        if (follow) {
            guiFAB.setImageResource(R.drawable.ic_baseline_location_disabled_24);
            follow = !follow;
        } else {
            guiFAB.setImageResource(R.drawable.ic_baseline_my_location_24);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker1.getPosition(), 18));
            follow = !follow;
        }
    }
    // Obsługa tworzenia Snackbarów podpowiedzi menu wybranej drużyny
    public void SelectedTeamMenu(View view) {
        SmacznySnackbar smacznySnackbar = new SmacznySnackbar(this, view);
        switch (CurrentUser.getInstance().getTeamID_S()) {
            case "Spectator":
                smacznySnackbar.MakeSmaczny(R.color.turquoise, R.drawable.ic_baseline_supervised_user_circle_24, R.string.team);
                break;
            case "TeamA":
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_supervised_user_circle_24, R.string.team);
                break;
            case "TeamB":
                smacznySnackbar.MakeSmaczny(R.color.blue, R.drawable.ic_baseline_supervised_user_circle_24, R.string.team);
                break;
            default:
                break;
        }
    }
    // Obsługa tworzenia Snackbarów podpowiedzi menu wybranego pseudonimu
    public void SelectedNicknameMenu(View view) {
        SmacznySnackbar smacznySnackbar = new SmacznySnackbar(this, view);
        switch (CurrentUser.getInstance().getTeamID_S()) {
            case "Spectator":
                smacznySnackbar.MakeSmaczny(R.color.turquoise, R.drawable.ic_baseline_emoji_people_24, R.string.user_name);
                break;
            case "TeamA":
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_emoji_people_24, R.string.user_name);
                break;
            case "TeamB":
                smacznySnackbar.MakeSmaczny(R.color.blue, R.drawable.ic_baseline_emoji_people_24, R.string.user_name);
                break;
            default:
                break;
        }
    }
    // Obsługa tworzenia Snackbarów podpowiedzi menu wybranego kodu gry
    public void SelectedGamecodeMenu(View view) {
        SmacznySnackbar smacznySnackbar = new SmacznySnackbar(this, view);
        switch (CurrentUser.getInstance().getTeamID_S()) {
            case "Spectator":
                smacznySnackbar.MakeSmaczny(R.color.turquoise, R.drawable.ic_baseline_videogame_asset_24, R.string.game_code);
                break;
            case "TeamA":
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_videogame_asset_24, R.string.game_code);
                break;
            case "TeamB":
                smacznySnackbar.MakeSmaczny(R.color.blue, R.drawable.ic_baseline_videogame_asset_24, R.string.game_code);
                break;
            default:
                break;
        }
    }
    // Obsługa kliknięcia przycisku wstecz
    @Override
    public void onBackPressed() {
        // Usuwanie zarezerwowanego użytkownika
        DatabaseReference firebaseReference = database.getReference("Group");
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("/" + CurrentUser.getInstance().getTeamID_S() + "/" + CurrentUser.getInstance().getUserID(),null);
        hashMap.put("/Spectator/" + CurrentUser.getInstance().getUserID(),null);
        firebaseReference.child(CurrentUser.getInstance().getJoinCode()).updateChildren(hashMap);

        stopGPS = true;
        wereActive = false;
        finish();
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        // Ustawianie stylu mapy na customowy
        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        } catch (Resources.NotFoundException e) { /* W przypadku źle ustawionego stylu pomija ten krok zamiast wywalić apke */}

        // Marker rozpoczęcia aktywności
        LatLng def = new LatLng(0,0);
        marker1 = mMap.addMarker(new MarkerOptions().position(def));
        marker1.setVisible(false);

        // TODO !! Hardcdoded markery, zmienić kiedyś na listę !!
        marker2 = mMap.addMarker(new MarkerOptions().position(def).icon(BitmapDescriptorFactory.fromResource(R.drawable.player_orange_32)));
        marker3 = mMap.addMarker(new MarkerOptions().position(def).icon(BitmapDescriptorFactory.fromResource(R.drawable.player_orange_32)));
        marker4 = mMap.addMarker(new MarkerOptions().position(def).icon(BitmapDescriptorFactory.fromResource(R.drawable.player_orange_32)));
        marker5 = mMap.addMarker(new MarkerOptions().position(def).icon(BitmapDescriptorFactory.fromResource(R.drawable.player_orange_32)));

        // Ustawianie nicku na pinezce
        marker1.setTitle(CurrentUser.getInstance().getNickname());
    }

    public void findSetReserveFirstEmptyUserId() {
        @Deprecated
        DatabaseReference firebaseSpectatorRef = database.getReference("Group/" + CurrentUser.getInstance().getJoinCode() + "/Spectator" );
        firebaseSpectatorRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // Sprawdzanie aktywnych użytkowników i nadawanie im unikalnego ID
                if (snapshot.exists()) {
                    int i = 0;
                    boolean success = false;
                    List<Integer> existingUsers = new ArrayList<>();
                    for (DataSnapshot child : snapshot.getChildren()) {
                        existingUsers.add(Integer.parseInt(Objects.requireNonNull(child.getKey()).substring(4)));
                    }
                    while (!success) {
                        success = !existingUsers.contains(i);
                        i++;
                    }
                    i--;
                    CurrentUser.getInstance().setUserID("User"+i);
                } else {
                    CurrentUser.getInstance().setUserID("User0");}
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
    // Obsługa zabicia procesu
    @Override
    protected void onDestroy() {
        if (CurrentUser.getInstance() != null) {
            // Usuwanie zarezerwowanego użytkownika
            FirebaseDatabase.getInstance("https://field-c9fb0-default-rtdb.europe-west1.firebasedatabase.app/");
            DatabaseReference firebaseReference = database.getReference("Group");
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("/" + CurrentUser.getInstance().getTeamID_S() + "/" + CurrentUser.getInstance().getUserID(),null);
            hashMap.put("/Spectator/" + CurrentUser.getInstance().getUserID(),null);
            firebaseReference.child(CurrentUser.getInstance().getJoinCode()).updateChildren(hashMap);
        }
        super.onDestroy();
    }
    // Funkcja kolorowania interfejsu
    public void paintColor(int color) {
        // Przypisanie dolnego interfejsu
        Toolbar gui = findViewById(R.id.bottom_info);
        FloatingActionButton guiFAB = findViewById(R.id.floating_follow);
        Button sNick = findViewById(R.id.selected_nickname);
        Button sCode = findViewById(R.id.selected_gamecode);
        // Kolorowanie dolnego interfejsu
        getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        getWindow().setNavigationBarColor(ContextCompat.getColor(this, color));
        gui.setBackgroundColor(getResources().getColor(color, this.getTheme()));
        guiFAB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, color)));
        sNick.setText(CurrentUser.getInstance().getNickname());
        sCode.setText(CurrentUser.getInstance().getJoinCode());
    }
}