package com.uaint.field.users;

public final class CurrentUser extends FirebaseUserDriver {

    private static volatile CurrentUser INSTANCE = null;

    // Tutaj dodawać nowe pola w klasie
    private String joinCode = "????";
    private String nickname = "DefaultCurrent";
    private String userID = "nullCurrent";
    // TeamID: 0 = spectator, 1 = red, 2 blue
    public int teamID;
    // ---

    // Fragment dotyczący singletone
    private CurrentUser() {}

    public static CurrentUser getInstance() {
        if (INSTANCE == null) {
            synchronized (CurrentUser.class) {
                if (INSTANCE == null) {
                    INSTANCE = new CurrentUser();
                }
            }
        }
        return INSTANCE;
    }
    // ---


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getJoinCode() {
        return joinCode;
    }

    public void setJoinCode(String joinCode) {
        this.joinCode = joinCode;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTeamID() {
        return teamID;
    }

    public String getTeamID_S() {
        if (teamID == 0) {return "Spectator";}
        if (teamID == 1) {return "TeamA";}
        if (teamID == 2) {return "TeamB";}
        return null;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    //TODO ZROBIĆ METODĘ GET DLA ENTRY DO FIREBASE ZE WSZYSTKIMI DANYMI
}
