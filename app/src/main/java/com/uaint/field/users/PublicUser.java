package com.uaint.field.users;

public class PublicUser extends FirebaseUserDriver {

    // Tutaj dodawać nowe pola w klasie
    public String nickname;
    public String userID;

    public PublicUser(String nickname, String userID) {
        this.nickname = nickname;
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public String getNickname() {
        return nickname;
    }
}
