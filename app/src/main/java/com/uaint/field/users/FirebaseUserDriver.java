package com.uaint.field.users;

import android.provider.ContactsContract;
import android.util.Log;

import androidx.annotation.Discouraged;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseUserDriver {
    private final static FirebaseDatabase database = FirebaseDatabase.getInstance("https://field-c9fb0-default-rtdb.europe-west1.firebasedatabase.app/");
    private static final String activeTeamsPath = "Group/ActiveTeams";
    private static String gameCodePath = "Group/NOT_SET";
    private static boolean setGameCodePath = false;
    private static String teamPath = gameCodePath + "/NOT_SET";
    private static boolean setTeamPath = false;

    // Aktualny user
    private static String currentUserPath = teamPath + "/NOT_SET";
    private static boolean setCurrentUserPath = false;
    private static String gpsCurrUsrPath = currentUserPath + "/GPS";
    private static boolean setGpsCurrUsrPath = false;
    // User publiczny
    private String publicUserReferencePath = teamPath + "/NOT_SET";
    private boolean setPublicUserPath = false;
    private String gpsPubUsrPath = publicUserReferencePath + "/GPS";
    private static boolean setGpsPubUsrPath = false;

    // Tutaj dodawać ścieżki w DB
    private final static DatabaseReference groupReference = database.getReference("Group");
    private final static DatabaseReference activeTeamsReference = database.getReference(activeTeamsPath);
    private static DatabaseReference gameCodeReference = database.getReference(gameCodePath);
    private static DatabaseReference teamReference = database.getReference(teamPath);
    private static DatabaseReference currentUserReference = database.getReference(currentUserPath);
    private static DatabaseReference gpsCurrUsrReference = database.getReference(gpsCurrUsrPath);
    private DatabaseReference publicUserReference = database.getReference(publicUserReferencePath);

    @Deprecated
    private DatabaseReference gpsReference = database.getReference(gpsCurrUsrPath);

    public FirebaseUserDriver() {}

    // --- Getter/Setter ---
    // Database - getter only
    public static FirebaseDatabase getDatabase() {
        return database;
    }

    // ActiveTeams - getter only
    public static DatabaseReference getActiveTeamsReference() {
        return activeTeamsReference;
    }

    // GameCodePath
    public static String getGameCodePath() throws Exception {
        if (setGameCodePath) {return gameCodePath;} else {throw new Exception("Game code path was not set!!");}
    }

    public static void setGameCodePath(String gameCodePath, boolean isFullPathProvided) {
        setGameCodePath = true;

        if (isFullPathProvided) {
            FirebaseUserDriver.gameCodePath = gameCodePath;
        } else {
            FirebaseUserDriver.gameCodePath = "Group/" + gameCodePath.replace("/","");
        }
        FirebaseUserDriver.gameCodeReference = database.getReference(FirebaseUserDriver.gameCodePath);
    }

    // TeamPath
    public static String getTeamPath() throws Exception {
        if (setTeamPath && setGameCodePath) {return teamPath;} else {throw new Exception("Game code or Team was not set!! Team: " + setTeamPath + "     Game code: " + setGameCodePath);}
    }

    public static void setTeamPath(String teamPath, boolean isFullPathProvided) {
        setTeamPath = true;

        if (isFullPathProvided) {
            FirebaseUserDriver.teamPath = teamPath;
        } else if (setGameCodePath) {
            FirebaseUserDriver.teamPath = gameCodePath + "/" + teamPath.replace("/","");
        } else {
            setTeamPath = false;
            Log.w("FirebaseUserDriver", "setGameCodePath was: " + setGameCodePath + " Expected TRUE! Ensure that GameCode path was set.");
        }
        FirebaseUserDriver.teamReference = database.getReference(FirebaseUserDriver.teamPath);
    }

    // CUsr
    public static String getCurrentUserPath() throws Exception {
        if (setCurrentUserPath && setTeamPath && setGameCodePath) {return currentUserPath;} else {throw new Exception("One of the following was not set!! Team: " + setTeamPath + "     Game code: " + setGameCodePath + "      CUsr path: " + setCurrentUserPath);}
    }

    public static void setCurrentUserPath(String currentUserPath, boolean isFullPathProvided) {
        setCurrentUserPath = true;

        if (isFullPathProvided) {
            FirebaseUserDriver.currentUserPath = currentUserPath;
        } else if (setTeamPath) {
            FirebaseUserDriver.currentUserPath = teamPath + "/" + currentUserPath.replace("/","");
        }

        FirebaseUserDriver.currentUserReference = database.getReference(currentUserPath);
    }

    // GPS CUsr
    public static String getGpsCurrUsrPath() throws Exception {
        if (setTeamPath && setGameCodePath && setGpsCurrUsrPath) {return gpsCurrUsrPath;} else {throw new Exception("Team/GameCode/GPSc was not set!! Team: " + setTeamPath + "     Game code: " + setGameCodePath + "   GPS: " + setGpsCurrUsrPath);}
    }

    public static void setGpsCurrUsrPath(String gpsCurrUsrPath, boolean isFullPathProvided) {
        setGpsCurrUsrPath = true;

        if (isFullPathProvided) {
            FirebaseUserDriver.gpsCurrUsrPath = gpsCurrUsrPath;
        } else if (setCurrentUserPath) {
            FirebaseUserDriver.gpsCurrUsrPath = currentUserPath + "/" + gpsCurrUsrPath.replace("/","");
        } else {
            setGpsCurrUsrPath = false;
            Log.w("FirebaseUserDriver", "setGpsCurrUsrPath was: " + setGpsCurrUsrPath + " Expected TRUE! Ensure that CurrentUserPath path was set.");
        }

        FirebaseUserDriver.gpsCurrUsrReference = database.getReference(FirebaseUserDriver.gpsCurrUsrPath);
    }

    // GPS PUsr
    @Discouraged(message = "Not yet implemented")
    public String getGpsPubUsrPath() throws Exception {
        if (setTeamPath && setGameCodePath && setGpsPubUsrPath) {return gpsPubUsrPath;} else {throw new Exception("Team/GameCode/GPSp was not set!! Team: " + setTeamPath + "     Game code: " + setGameCodePath + "   GPS: " + setGpsPubUsrPath);}
    }

    @Deprecated
    public void setGpsPath(String gpsPath, boolean isUserProvided) {
        if (isUserProvided && setTeamPath) {
            setGpsCurrUsrPath = true;
            setGpsPubUsrPath = true;
            FirebaseUserDriver.gpsCurrUsrPath = teamPath + "/" + gpsPath;
            this.gpsPubUsrPath = teamPath + "/" + gpsPath;
        } else return;
        gpsReference = database.getReference(FirebaseUserDriver.gpsCurrUsrPath);
    }

    // TODO Zrobić prywatne metody dla klasy CUser i PUser na ustawianie ścieżek dla GPS
    // TODO Zrobić reszte getterów i setterów
    // TODO Zrobić ścieżkę dla nicków

}
