package com.uaint.field;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uaint.field.users.CurrentUser;
import com.uaint.field.users.FirebaseUserDriver;

public class MainActivity extends AppCompatActivity {
    // Zmienna dla przycisku debugowania
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        // Ptaszek na klawiaturze wywołuje metodę przypisaną do przycisku dołączania
        EditText codeNumber = findViewById(R.id.game_code);
        codeNumber.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_DONE){
                JoinClick(v);
            }
            return false;
        });
    }
    // Obsługa przycisku tworzenia gry
    public void CreateClick (View v) {
        SmacznySnackbar smacznySnackbar = new SmacznySnackbar(this, v);
        smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_warning_24, R.string.error_create);
    }
    // Obsługa przycisku dołączania do gry
    public void JoinClick (View v){
        // Odczytywanie wartości z EditTExtów
        EditText nickname = findViewById(R.id.user_name);
        EditText codeNumber = findViewById(R.id.game_code);
        String nickname_s = nickname.getText().toString();
        String codeNumber_s = codeNumber.getText().toString();
        SmacznySnackbar smacznySnackbar = new SmacznySnackbar(this, v);
        // Walidacja danych metodą własną
        switch(isValid(nickname_s,codeNumber_s)) {
            case 0:
                // Sprawdzenie wartości w Firebase
                FirebaseUserDriver.getActiveTeamsReference().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        // Pobranie listy aktywnych drużyn
                        String activeTeams = snapshot.getValue().toString();
                        // Odczytywanie wartości z EditTextów
                        EditText nickname = findViewById(R.id.user_name);
                        EditText codeNumber = findViewById(R.id.game_code);
                        // Zamiana tych wartości na String
                        String nicknameS = nickname.getText().toString();
                        String codeNumberS = codeNumber.getText().toString();
                        // Sprawdzenie czy drużyna istnieje w bazie danych
                        if (activeTeams.contains(codeNumberS)) {
                            // Ustawianie nicku i kodu w User
                            CurrentUser.getInstance().setNickname(nicknameS);
                            CurrentUser.getInstance().setJoinCode(codeNumberS);
                            FirebaseUserDriver.setGameCodePath(codeNumberS,false);
                            try {
                                Log.i("MainActivity, FirebaseUserDriver for game code path:", FirebaseUserDriver.getGameCodePath());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Uruchamianie activity -> Wybór drużyn
                            startActivity(new Intent(MainActivity.this, TeamSelectActivity.class));
                        } else smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_warning_24, R.string.code_nonexistant);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {}});
                break;
            case 1:
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_warning_24, R.string.empty_nickname_info);
                break;
            case 2:
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_warning_24, R.string.empty_code_info);
                break;
            case 3:
                smacznySnackbar.MakeSmaczny(R.color.red, R.drawable.ic_baseline_warning_24, R.string.too_short_code_info);
                break;
            default:
                break;
        }
    }
    // Sprawdza poprawność wpisanych danych - zwraca 0 przy prawidłowych danych i dwa kody błedu przy złych wartościach
    private byte isValid(String nickname_s, String codeNumber_s) {
        if (nickname_s == null || nickname_s.isEmpty()) { return 1; }   // Pusty nickname
        if (codeNumber_s == null || codeNumber_s.isEmpty()) { return 2; }   // Pusty codenumber
        if (codeNumber_s.length() != 4) { return 3; }   // Za krótki codenumber
        return 0;
    }
}